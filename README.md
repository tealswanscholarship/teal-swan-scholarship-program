The Teal Swan Scholarship Program will award two scholarships of $500 USD each.

Students currently enrolled at a university or college in North America, Europe, and Australia are eligible to apply.

Students must be specializing in philosophy, psychology, or religious studies to be eligible.

Deadline is January 10, 2020.

Website : http://www.tealswanscholarships.com/
